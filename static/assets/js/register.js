$(document).ready(function(){
	/* Signup function*/
	$('#create_account').click(function(){
		var name = $('#user_name').val();
		var email = $('#user_email').val();
		var password = $('#user_password').val();
		var cnf_password = $('#user_cnf_password').val();
		console.log('>>',name,email,password,cnf_password)
		if((name == '') || (email == '') || (password == '') || (cnf_password == '')){
			$("#empty_fields_error").show().delay(5000).fadeOut();
		}else{
			console.log('all found')
			is_valid_email = isEmail(email)
			console.log('is_valid_email',is_valid_email)
			if(is_valid_email == true){
				if(password.length >= 8){
					if(password != cnf_password){
						$("#password_mismatch_error").show().delay(5000).fadeOut();
					}else{
						console.log('start ajax here')
						$.ajax({
						     url: '/users/register/',
						     type: 'POST',
						     data: {
						     	'name':name,
						     	'email':email,
						     	'password':password
						     },
						     success: function(data) {    
						        console.log('success')
						     },
						     failure: function(data) { 
						         alert('Got an error dude');
						     }
						});
					}
				}else{
					$("#password_length_error").show().delay(5000).fadeOut();
				}
			}else{
				$("#invalid_email_error").show().delay(5000).fadeOut();
			}
		}
	})


	/*Restrict numbers in name field*/
	$("#user_name").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
    });

    /* Validate email address*/
    function isEmail(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}
})