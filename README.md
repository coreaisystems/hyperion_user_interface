################Hyperion User Interface MicroService#########################

To see the functionality please follow following steps:

1) CD to the project folder

2) run the command to build :
	docker-compose up --build

3) Application will be running on port 5000

4) Navigate to 127:0.0.0:5000
